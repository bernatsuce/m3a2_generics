package M3.GenericsEX;

public class Videojuegos implements Comparable<Videojuegos>{
	
	private String nombre;
	private int anyocreacion;
	private int Pegi;
	
	public Videojuegos() {
		super();
	}

	

	public Videojuegos(String nombre, int anyocreacion, int Pegi) {
		super();
		this.nombre = nombre;
		this.anyocreacion = anyocreacion;
	}

	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public int getAnyocreacion() {
		return anyocreacion;
	}



	public void setAnyocreacion(int anyocreacion) {
		this.anyocreacion = anyocreacion;
	}



	public int getPegi() {
		return Pegi;
	}



	public void setPegi(int pegi) {
		Pegi = pegi;
	}


	@Override
	public String toString() {
		return "Videojuegos [nombre=" + nombre + ", anyocreacion=" + anyocreacion + ", Pegi=" + Pegi + "]";
	}

	@Override
	public int compareTo(Videojuegos o) {
		// Ordenacio ascendent nom
		//return (this.nombre.compareTo(o.nombre));
		// Ordenacion descendente nom
		return (o.nombre.compareTo(this.nombre));
		// Ordenacion descendente Any
		//return o.anyocreacion - this.anyocreacion;
		
	}
	
	

}
