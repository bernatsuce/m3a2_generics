package M3.GenericsEX;

public class Family<T> {
	
	private T Father;
	private T Mother;
	private T[] Child;
	
	public Family() {
		super();
	}
	
	public Family(T Father, T Mother, T[] Child) {
		//ACABAR DE LLENAR
		super();
		this.Father = Father;
		this.Mother = Mother;
		this.Child = Child;
	}


	public T getFather() {
		return Father;
	}

	public void setFather(T father) {
		Father = father;
	}

	public T getMother() {
		return Mother;
	}

	public void setMother(T mother) {
		Mother = mother;
	}

	public T getChild(int i) {
		return Child[i];
	}

	public void setChild(T[] child) {
		Child = child;
	}

	public String print() {
		return "Family [Father=" + Father + ", Mother=" + Mother + ", Child=" + Child + "]";
	}
	
	
	
	
}
