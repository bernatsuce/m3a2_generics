package M3.GenericsEX;

public class Human {

	private String nombre;
	private int edad;
	
	public Human() {
		super();
	}
	
	public Human(String nombre, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "Human [nombre=" + nombre + ", edad=" + edad + "]";
	}

	public String print() {
		return "Human [nombre=" + nombre + ", edad=" + edad + "]";
	}

	
}
