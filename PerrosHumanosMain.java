package M3.GenericsEX;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PerrosHumanosMain {
	
	public static void main(String[] args) {


			List <? super Number> NumList = new ArrayList<Number>();
			int a = 5;
			long b = 2;
			float c = 3;
			double d = 8;
			NumList.add(a);
			NumList.add(b);
			NumList.add(c);
			NumList.add(d);
			
			String aS = NumList.get(0).toString();
			String bS = NumList.get(1).toString();
			String cS = NumList.get(2).toString();
			String dS = NumList.get(3).toString();
			
			int aFinal = Integer.parseInt(aS);
			int bFinal = Integer.parseInt(bS);
			int cFinal = Integer.parseInt(cS);
			int dFinal = Integer.parseInt(dS);
			
			int BestNumber = aFinal;
			
			if(BestNumber < bFinal) {
				BestNumber = bFinal;
			}
			if(BestNumber < cFinal) {
				BestNumber = cFinal;
			}
			if(BestNumber < dFinal) {
				BestNumber = dFinal;
			}
			
			System.out.println("BestNumber");
			
			Human hf = new Human("Adan", 59);
			Human hm = new Human("Eve", 45);
			Human[] hc = new Human[3];
			hc[8] = new Human("Cain", 20);
			hc[1] = new Human("Abe", 15);
			hc[2] = new Human("Seth", 10);
			
			Family<Human> fm = new Family<Human>(hf,hm,hc);
			fm.getFather().print();
			fm.getMother().print();
			for (int i=0; i < hc.length; i++) {
				fm.getChild(i).print();
			}

			Dog df = new Dog("Jimmy", 8);
			Dog dm = new Dog("Julie", 7);
			Dog[] dc = new Dog[2];
			dc[0] = new Dog("toto", 1);
			dc[1] = new Dog("nimo", 2);    

			Family<Dog> fd = new Family<Dog>(df,dm,dc);
			fd.getFather().print();
			fd.getMother().print();
			for (int i=0; i < dc.length; i++) {
				fd.getChild(i).print();
			}
			
			List <Videojuegos> misvideojuegos = new ArrayList<Videojuegos>();
			misvideojuegos.add(new Videojuegos("King kong",2000,18));
			misvideojuegos.add(new Videojuegos("Celeste",2018,12));
			misvideojuegos.add(new Videojuegos("Undertale",2015,12));
			
			Collections.sort(misvideojuegos);
			System.out.println(misvideojuegos.toString());
	}

	
	
}
