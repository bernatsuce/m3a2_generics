package M3.GenericsEX;

public class Dog {

	private String nombre;
	private int edad;
	
	public Dog() {
		super();
	}
	
	public Dog(String nombre, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "Dog [nombre=" + nombre + ", edad=" + edad + "]";
	}

	public String print() {
		return "Dog [nombre=" + nombre + ", edad=" + edad + "]";
	}
	
	

}
